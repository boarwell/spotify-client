import * as conf from './conf.js';

// URL内に記載されているaccess_tokenを取得する
const getFromHash = (hash:string) => { 
 // hash: #access_token=NwAExz...BV3O2Tk&token_type=Bearer&expires_in=3600

  for (const el of hash.slice(1).split('&')) {
    const i = el.indexOf('=');
    if (el.slice(0, i) !== 'access_token') {
      continue
    }
    return el.slice(i+1)
  }

  return ''
}

// localStrage か URLからaccess_tokenを取得して返す
// どちらにも設定されていなかったら空文字列が返る
export const getExistingOne = () => {
  const ct = localStorage.getItem(conf.cachedToken);
  if (ct) {
    return ct;
  }

  const ht = getFromHash(window.location.hash)
  localStorage.setItem(conf.cachedToken, ht);
  return ht;
}


// 新しいトークンを作成する
export const update = () => {
  localStorage.removeItem(conf.cachedToken);
  const e = encodeURIComponent(conf.appURL);
  location.href = `https://accounts.spotify.com/authorize?client_id=${conf.clientID}&response_type=token&redirect_uri=${e}&scope=user-read-playback-state`
}

export const removeHashFromURL = () => {
  history.replaceState({}, document.title, location.pathname)
}