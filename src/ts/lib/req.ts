import * as conf from "./conf.js";

// https://developer.mozilla.org/en-US/docs/Web/API/ReadableStreamDefaultReader/read
interface StreamReadValue {
  value: Uint8Array,
  done: boolean
}

// TODO: access_tokenが有効期限切れだったときのエラー処理
export const currentPlayback = async (token:string) => {
  const res = await fetch(conf.currentPlaybackEP, {
    headers: {
      Authorization: `Bearer ${token}`
    },
    mode: "cors"
  });

  // res.body はFirefoxで実装されていないらしくエラーになる
  // https://www.reddit.com/r/firefox/comments/6imnb9/need_help_with_using_fetch_api_in_firefox/
  if (res.body === null) {
    return {};
  }

  const b:StreamReadValue = await res.body.getReader().read();
  return JSON.parse(new TextDecoder().decode(b.value));
};
