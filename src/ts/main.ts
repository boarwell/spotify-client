import * as req from "./lib/req.js";
import * as token from "./lib/token.js";

const t = token.getExistingOne();
if (t === "") {
  token.update();
}
token.removeHashFromURL();

const render = async () => {
  const v = await req.currentPlayback(t);
  vm.json = v;
  // console.log(v); // デバッグ用
};

const vm = new Vue({
  el: "main",
  data: {
    json: {
      item: {
        name: "LOADING...",
        artists: [
          {
            name: "LOADING..."
          }
        ],
        album: {
          name: "LOADING...",
          images: [
            {
              url: ""
            }
          ]
        }
      }
    }
  },

  methods: {
    updateToken: token.update,
    reload: render
  },

  computed: {
    // プログレスバーの表示
    percentage():(void | number) {
      const res = this.json.progress_ms / this.json.item.duration_ms;
      if (isNaN(res)) {
        return
      }
      return res;
    }
  }
});

// 初回アクセス時の描画を実行
(async () => {
  render();
})();


setInterval(render, 5000);
// 30分ごとにaccess_tokenを更新
// 有効期限は1時間らしい
// https://developer.spotify.com/documentation/general/guides/authorization-guide/#implicit-grant-flow
setInterval(token.update, 1000 * 60 * 30);
